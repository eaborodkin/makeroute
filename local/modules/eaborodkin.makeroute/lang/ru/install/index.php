<?php

$MESS['EABORODKIN_MAKEROUTE_MODULE_NAME'] = 'Модуль построения эффективных маршрутов';
$MESS['EABORODKIN_MAKEROUTE_MODULE_DESCRIPTION'] = 'Модуль построения эффективных маршрутов между городами';
$MESS['EABORODKIN_MAKEROUTE_INSTALL_TITLE'] = 'Установка модуля "Модуль построения эффективных маршрутов"';
$MESS['EABORODKIN_MAKEROUTE_UNINSTALL_TITLE'] = 'Удаление модуля "Модуль построения эффективных маршрутов"';
$MESS['EABORODKIN_MAKEROUTE_MODULE_INSTALLED'] = 'Модуль "Модуль построения эффективных маршрутов" уже установлен в системе!';
$MESS['EABORODKIN_MAKEROUTE_MODULE_VERSION_REQUIREMENT'] = 'Необходима версия ядра 1С-Битрикс 14+. Т.к. модуль использует ядро D7!';
