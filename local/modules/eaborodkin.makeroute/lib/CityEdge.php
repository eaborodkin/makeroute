<?php

namespace Eaborodkin\MakeRoute;


/**
 * Класс реализующий сущность отношения между двумя населёнными пунктами
 * @package Eaborodkin\MakeRoute
 */
class CityEdge extends Edge
{
    /**
     * Конструктор CityEdge
     * @param City $source
     * @param City $target
     */
    public function __construct(City $source, City $target)
    {
        parent::__construct($source, $target, new CityWeightValue($source, $target));
    }
}
