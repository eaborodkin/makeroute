<?php

namespace Eaborodkin\MakeRoute;


/**
 * Класс реализующий сущность ребра в графе между двумя вершинами
 * @package Eaborodkin\MakeRoute
 */
class Edge
{
    /**
     * Вершина отправления
     * @var Vertex
     */
    protected $source;
    /**
     * Вершина назначения
     * @var Vertex
     */
    protected $target;
    /**
     * Вес ребра
     * @var WeightValue
     */
    protected $weight;

    /**
     * Возвращает объект вершины отправления
     * @return Vertex
     */
    public function getSource(): Vertex
    {
        return $this->source;
    }

    /**
     * Возвращает объект вершины назначения
     * @return Vertex
     */
    public function getTarget(): Vertex
    {
        return $this->target;
    }

    /**
     * Возвращает объект веса ребра
     * @return WeightValue
     */
    public function getWeight(): WeightValue
    {
        return $this->weight;
    }

    /**
     * Конструктор объекта ребра
     * @param Vertex $source
     * @param Vertex $target
     * @param WeightValue $weight
     */
    public function __construct(Vertex $source, Vertex $target, WeightValue $weight)
    {
        $this->source = $source;
        $this->target = $target;
        $this->weight = $weight;
    }
}