<?php

namespace Eaborodkin\MakeRoute;


/**
 * Класс реализующий сущность графа
 * @package Eaborodkin\MakeRoute
 */
class GraphCollection implements \Countable, \Iterator
{
    /**
     * Объект класса SplObjectStorage для хранения отношения "откуда" в все возможные "куда"
     * @var \SplObjectStorage
     */
    protected $instance;

    /**
     * Конструктор графа
     */
    public function __construct()
    {
        $this->instance = new \SplObjectStorage();
    }

    /**
     * Добавляет ребро(связь между "откуда" и очередным "куда")
     * @param Edge $edge
     */
    public function attach(Edge $edge)
    {
        // Инициализация объекта типа SplObjectStorage для хранения связки объектов $destination <-> $weight
        $targetObjects = new \SplObjectStorage();

        // если в instanse уже есть записи о рёбрах по исходящей вершине, то заполняем ими временное хранилище $targetObjects
        if ($this->instance->offsetExists($edge->getSource())) {
            $targetObjects->addAll($this->instance->offsetGet($edge->getSource()));
        }


        $targetObjects->attach($edge->getTarget(), $edge->getWeight());


        $this->instance->attach($edge->getSource(), $targetObjects);
    }

    /**
     * @return \SplObjectStorage
     */
    public function current(): \SplObjectStorage
    {
        return $this->instance->offsetGet($this->key());
    }

    /**
     *
     */
    public function next(): void
    {
        $this->instance->next();
    }

    /**
     * @return Vertex
     */
    public function key(): Vertex
    {
        return $this->instance->current();
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return $this->instance->valid();
    }

    /**
     *
     */
    public function rewind(): void
    {
        $this->instance->rewind();
    }

    /**
     * Возвращает количество пунктов отправления
     * @return int
     */
    public function count(): int
    {
        return $this->instance->count();
    }

    /**
     * Существует ли указанный пункт отправления
     * @param Vertex $offset
     * @return bool
     * @throws \Exception
     */
    public function offsetExists($offset): bool
    {
        if (!($offset instanceof Vertex)) {
            throw new \Exception('Invalid type of the argument!');
        }

        return $this->instance->offsetExists($offset);
    }

    /**
     * Для указанного пункта отправления возвращает SplObjectStorage содержащий пункты назначения и веса
     * @param mixed $offset
     * @return \SplObjectStorage
     * @throws \Exception
     */
    public function offsetGet($offset): \SplObjectStorage
    {
        if (!($offset instanceof Vertex)) {
            throw new \Exception('Invalid type of the argument!');
        }

        return $this->instance->offsetGet($offset);
    }
}
