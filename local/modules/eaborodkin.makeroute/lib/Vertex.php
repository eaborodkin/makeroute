<?php

namespace Eaborodkin\MakeRoute;


/**
 * Класс реализующий базовую сущность вершины графа
 * @package Eaborodkin\MakeRoute
 */
class Vertex
{
    /**
     * Имя вершины
     * @var string
     */
    private $name;

    /**
     * Устанавливает имя вершины
     * @param string $name
     */
    protected function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Возвращает имя вершины
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Конструктор вершины
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->setName($name);
    }

    /**
     * При обращении к объекту как к строке вернёт имя вершины
     * @return string
     */
    public function __toString():string
    {
        return $this->getName();
    }
}