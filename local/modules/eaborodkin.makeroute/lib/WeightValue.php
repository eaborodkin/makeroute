<?php

namespace Eaborodkin\MakeRoute;


/**
 * Базовый класс реализации сущности веса ребра. Нужен для возможного вычисления веса ребра.
 * @package Eaborodkin\MakeRoute
 */
class WeightValue
{
    /**
     * Вес ребра
     * @var float
     */
    private $value;

    /**
     * Возвращает вес ребра
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * Устанавливает вес ребра
     * @param float $value
     */
    protected function setValue(float $value): void
    {
        $this->value = $value;
    }

    /**
     * Конструктор сущности веса ребра
     * @param float $value
     */
    public function __construct(float $value)
    {
        $this->setValue($value);
    }
}