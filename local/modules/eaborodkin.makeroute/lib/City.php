<?php

namespace Eaborodkin\MakeRoute;


/**
 * Класс реализующий сущность населённого пункта с географическими координатами
 * @package Eaborodkin\MakeRoute
 */
class City extends Vertex
{
    /**
     * широта
     * @var float
     */
    private $latitude;

    /**
     * долгота
     * @var float
     */
    private $longitude;

    /**
     * Возвращает широту
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * Устанавливает широту
     * @param float $latitude
     */
    protected function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * Возвращает долготу
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * Устанавливает долготу
     * @param float $longitude
     */
    protected function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * Конструктор City
     * @param string $name
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct(string $name, float $latitude, float $longitude)
    {
        $this->setLatitude($latitude);
        $this->setLongitude($longitude);
        parent::__construct($name);
    }
}