<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Eaborodkin\MakeRoute\Dijkstra;
use Eaborodkin\MakeRoute\Edge;
use Eaborodkin\MakeRoute\GraphCollection;
use Eaborodkin\MakeRoute\Vertex;
use Eaborodkin\MakeRoute\WeightValue;
use Bitrix\Main\SystemException as Exception;

/**
 * Class MakeRoute
 */
class MakeRoute extends CBitrixComponent
{

    /**
     * @inheritdoc
     * @throws \Bitrix\Main\LoaderException
     */
    public function __construct(?CBitrixComponent $component = null)
    {
        Loc::loadMessages(__FILE__);

        parent::__construct($component);

        Loader::includeModule('eaborodkin.makeroute');
    }

    /**
     * Функция поиска самого дешевого маршрута
     *
     * @param string $from населённый пункт отправления
     * @param string $to населённый пункт назначения
     * @param array $arRoutes массив маршрутов с элементами вида [откуда, куда, стоимость]
     * @return array
     * @throws \Exception
     */
    protected function getCheapestRoute(string $from, string $to, array $arRoutes): array
    {
        // Будущий массив населённых пунктов вида: населённый пункт => объект населённого пункта
        $arVertexObjects = [];

        $oGraph = new GraphCollection();

        $arVertexObjects[$from] = new Vertex($from);
        $arVertexObjects[$to] = new Vertex($to);

        if (!is_string($from) || empty($from)) {
            throw new Exception(Loc::getMessage('EABORODKIN_MAKEROUTE_COMPONENT_TEMPLATE_EXCEPTION_FROM'));
        }
        if (!is_string($to) || empty($to)) {
            throw new Exception(Loc::getMessage('EABORODKIN_MAKEROUTE_COMPONENT_TEMPLATE_EXCEPTION_TO'));
        }
        foreach ($arRoutes as $k => $arRoute) {
            if (!is_string($arRoute[0]) || empty($arRoute[0])) {
                throw new Exception(str_replace('#NUMBER#', $k, Loc::getMessage('EABORODKIN_MAKEROUTE_COMPONENT_TEMPLATE_EXCEPTION_ROUTE_FROM')));
            }
            if (!is_string($arRoute[1]) || empty($arRoute[1])) {
                throw new Exception(str_replace('#NUMBER#', $k, Loc::getMessage('EABORODKIN_MAKEROUTE_COMPONENT_TEMPLATE_EXCEPTION_ROUTE_TO')));
            }
            if (!is_numeric($arRoute[2]) || empty($arRoute[2])) {
                throw new Exception(str_replace('#NUMBER#', $k, Loc::getMessage('EABORODKIN_MAKEROUTE_COMPONENT_TEMPLATE_EXCEPTION_ROUTE_PRICE')));
            }

            if (!in_array($arRoute[0], $arVertexObjects)) {
                $arVertexObjects[$arRoute[0]] = new Vertex($arRoute[0]);
            }
            if (!in_array($arRoute[1], $arVertexObjects)) {
                $arVertexObjects[$arRoute[1]] = new Vertex($arRoute[1]);
            }

            $oGraph->attach(new Edge($arVertexObjects[$arRoute[0]], $arVertexObjects[$arRoute[1]], new WeightValue($arRoute[2])));
            $oGraph->attach(new Edge($arVertexObjects[$arRoute[1]], $arVertexObjects[$arRoute[0]], new WeightValue($arRoute[2])));
        }

        $oDijkstra = new Dijkstra($oGraph);


        $arResult = $oDijkstra->shortestPath($arVertexObjects[$from], $arVertexObjects[$to]);

        /** @var \SplStack $optimalChainStack */
        $optimalChainStack = $arResult['chainStack'];
        $optimalChainTotalWeight = $arResult['totalWeight'];

        if (is_null($optimalChainStack)) {
            return [
                'chain' => [],
                'price' => $optimalChainTotalWeight
            ];
        }

        $arChains = [];
        while (!$optimalChainStack->isEmpty()) {
            $arChains[] = $optimalChainStack->pop();
        }

        return [
            'chain' => $arChains,
            'price' => $optimalChainTotalWeight
        ];
    }


    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function executeComponent()
    {
        if ($this->StartResultCache()) {
            $arResult = $this->getCheapestRoute(
                $this->arParams['from'],
                $this->arParams['to'],
                $this->arParams['graph']
            );

            $this->arResult['chain'] = $arResult['chain'];
            $this->arResult['price'] = $arResult['price'];

            $this->includeComponentTemplate();
        }
    }
}