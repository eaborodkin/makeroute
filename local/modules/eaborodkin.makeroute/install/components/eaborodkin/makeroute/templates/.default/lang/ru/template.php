<?php

$MESS['EABORODKIN_MAKEROUTE_COMPONENT_TEMPLATE_EXCEPTION_FROM'] = 'Населённый пункт отправления(1-й параметр функции) должен быть строкового типа и не пустой строкой.';
$MESS['EABORODKIN_MAKEROUTE_COMPONENT_TEMPLATE_EXCEPTION_TO'] = 'Населённый пункт назначения(2-й параметр функции) должен быть строкового типа и не пустой строкой.';
$MESS['EABORODKIN_MAKEROUTE_COMPONENT_TEMPLATE_EXCEPTION_ROUTE_FROM'] = 'В #NUMBER#-й строке массива маршрутов(3-й параметр функции) населённый пункт отправления должен быть строкового типа и не пустой строкой.';
$MESS['EABORODKIN_MAKEROUTE_COMPONENT_TEMPLATE_EXCEPTION_ROUTE_TO'] = 'В #NUMBER#-й строке массива маршрутов(3-й параметр функции) населённый пункт назначения должен быть строкового типа и не пустой строкой.';
$MESS['EABORODKIN_MAKEROUTE_COMPONENT_TEMPLATE_EXCEPTION_ROUTE_PRICE'] = 'В #NUMBER#-й строке массива маршрутов(3-й параметр функции) стоимость проезда от одного населенного пункта до другого должна быть целым или дробным числом, не пустым.';