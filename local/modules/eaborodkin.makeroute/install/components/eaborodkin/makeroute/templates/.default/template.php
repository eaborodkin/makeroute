<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

    Маршрут из <?= $arParams['from']; ?> в <?= $arParams['to']; ?>: <br>
<?php
if (!empty($arResult['chain'])) {
    foreach ($arResult['chain'] as $num => $element) { ?>
        <?= $element; ?>
        <?= $num < (count($arResult['chain']) - 1) ? ' → ' : ''; ?>
    <?php } ?><br>
    Стоимость проезда = <?= $arResult['price'] ?? '0';
} else { ?>
    Нет пути!
<?php }
