<?php

use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\SystemException;
use Bitrix\Main\IO\Directory;

class eaborodkin_makeroute extends CModule
{
    public $MODULE_ID = 'eaborodkin.makeroute';
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $PARTNER_NAME = 'Евгений Бородкин [eaborodkin@gmail.com]';
    public $MODULE_GROUP_RIGHTS = 'Y';

    public function __construct()
    {
        Loc::loadMessages(__FILE__);
        $arModuleVersion = array();
        include(static::getPath() . '/install/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('EABORODKIN_MAKEROUTE_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('EABORODKIN_MAKEROUTE_MODULE_DESCRIPTION');
    }

    /**
     * @throws SystemException
     */
    public function DoInstall()
    {
        global $APPLICATION;
        if (IsModuleInstalled($this->MODULE_ID)) {
            throw new SystemException(Loc::getMessage('EABORODKIN_MAKEROUTE_MODULE_INSTALLED'));
        }
        if (!static::isVersionD7()) {
            throw new SystemException(Loc::getMessage('EABORODKIN_MAKEROUTE_MODULE_VERSION_REQUIREMENT'));
        }

        $this->installFiles();
        $this->installEvents();
        $this->installDB();

        if (!ModuleManager::isModuleInstalled($this->MODULE_ID)) {
            ModuleManager::registerModule($this->MODULE_ID);
        }

        $APPLICATION->includeAdminFile(
            Loc::getMessage('EABORODKIN_MAKEROUTE_INSTALL_TITLE'),
            static::getPath() . '/install/step.php'
        );
    }

    public function installDB()
    {
        return true;
    }

    public function installEvents()
    {
        return true;
    }

    public function installFiles()
    {
        // копирование компонентов в /bitrix/components/
        CopyDirFiles(
            static::getPath() . '/install/components/',
            Application::getDocumentRoot() . '/bitrix/components/',
            true,
            true
        );

        // копирование демонстрационных скриптов
        CopyDirFiles(
            static::getPath() . '/install/makeroute_examples/',
            Application::getDocumentRoot() . '/makeroute_examples/',
            true,
            true
        );
    }

    public function doUninstall()
    {
        global $APPLICATION;

        $this->uninstallDB();
        $this->uninstallEvents();
        $this->uninstallFiles();

        if (ModuleManager::isModuleInstalled($this->MODULE_ID)) {
            ModuleManager::unRegisterModule($this->MODULE_ID);
        };

        $APPLICATION->includeAdminFile(
            Loc::getMessage('EABORODKIN_MAKEROUTE_MODULE_INSTALLED'),
            static::getPath() . '/install/unstep.php'
        );
    }

    public function uninstallDB()
    {
        return true;
    }

    public function uninstallEvents()
    {
        return true;
    }

    public function uninstallFiles()
    {
        // удаление компонентов модуля из /bitrix/components/
        Directory::deleteDirectory(Application::getDocumentRoot() . '/bitrix/components/eaborodkin/');

        // удаление демонстрационных скриптов
        Directory::deleteDirectory(Application::getDocumentRoot() . '/makeroute_examples/');
    }

    // Определяем место размещения модуля
    private static function getPath(bool $absolutePath= true): string
    {
        if ($absolutePath) {
            return dirname(__DIR__);
        } else {
            return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
        }
    }

    // Проверяем что система поддерживает ядро D7
    public static function isVersionD7(): bool
    {
        return CheckVersion(ModuleManager::getVersion('main'), '14.00.00');
    }
}
