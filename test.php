<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetTitle('');
try {
    $arRequest = Bitrix\Main\Application::getInstance()->getContext()->getRequest();

    $APPLICATION->IncludeComponent(
        'eaborodkin:makeroute',
        '',
        [
            'from' => $arRequest['from'] ?? 'C',
            'to' => $arRequest['to'] ?? 'A',
            'graph' => [
                ['A', 'B', 3],
                ['A', 'D', 3],
                ['A', 'F', 6],
                ['B', 'D', 1],
                ['B', 'E', 3],
                ['C', 'E', 2],
                ['C', 'F', 3],
                ['D', 'E', 1],
                ['D', 'F', 2],
                ['E', 'F', 5],
            ]
        ]
    );
} catch (Exception $exception) {
    echo $exception->getMessage();
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
$APPLICATION->SetTitle('');
